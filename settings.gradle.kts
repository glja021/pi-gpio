pluginManagement {
    repositories {
        maven {
            url = uri("https://dl.bintray.com/kotlin/kotlin-eap")
        }
        mavenCentral()
    }
}

rootProject.name = "pi-gpio"

